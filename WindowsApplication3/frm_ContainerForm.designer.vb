﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_ContainerForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_ContainerForm))
        Me.ContentsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.IndexToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.SearchToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.toolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator
        Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.NewToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.OpenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.toolStripSeparator = New System.Windows.Forms.ToolStripSeparator
        Me.SaveToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.SaveAsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.toolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.PrintToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.PrintPreviewToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.toolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.UndoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.RedoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.toolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator
        Me.CutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.CopyToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.PasteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.toolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator
        Me.SelectAllToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.CustomizeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.OptionsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.NewToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.OpenToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.toolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator
        Me.SaveToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.SaveAsToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.toolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator
        Me.PrintToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.PrintPreviewToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.toolStripSeparator8 = New System.Windows.Forms.ToolStripSeparator
        Me.ExitToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.UndoToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.RedoToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.toolStripSeparator9 = New System.Windows.Forms.ToolStripSeparator
        Me.CutToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.CopyToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.PasteToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.toolStripSeparator10 = New System.Windows.Forms.ToolStripSeparator
        Me.SelectAllToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.CustomizeToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.OptionsToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.ContentsToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.IndexToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.SearchToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.toolStripSeparator11 = New System.Windows.Forms.ToolStripSeparator
        Me.AboutToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
        Me.MainToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.LogToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.LogOutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator26 = New System.Windows.Forms.ToolStripSeparator
        Me.ChangeServerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ChangeCompanyToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator27 = New System.Windows.Forms.ToolStripSeparator
        Me.ToolStripSeparator28 = New System.Windows.Forms.ToolStripSeparator
        Me.ExitToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem
        Me.FILEToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.GETEXCELLToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.GETWORDDOCUMENTToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.GETTXTBYTRNNOToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.GETTXTBYDATEToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.AdminToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.CREATECOMPANYToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.CompanyParameterToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator18 = New System.Windows.Forms.ToolStripSeparator
        Me.UserInformationToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.UserRightsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator19 = New System.Windows.Forms.ToolStripSeparator
        Me.NeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.MasterToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.AccountCreationToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.AcountSubGroupToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.LedgerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.TaxShemesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator16 = New System.Windows.Forms.ToolStripSeparator
        Me.BrandMasterToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ItemCateToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.KindOfForeignLoquoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.MeasurePackingToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.StrengthToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ItemMasterToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.SalesRateToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator17 = New System.Windows.Forms.ToolStripSeparator
        Me.StorageLocationToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.REPORTToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.SalesReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.SaleReportsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.mnu_daily_sale = New System.Windows.Forms.ToolStripMenuItem
        Me.mnu_monthly_sale = New System.Windows.Forms.ToolStripMenuItem
        Me.mnu_d2d_sale = New System.Windows.Forms.ToolStripMenuItem
        Me.mnu_multybill_print = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem26 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem42 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem43 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem44 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem27 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem28 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem29 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem30 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem23 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem39 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem40 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem41 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem31 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem32 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem33 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem34 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem35 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem36 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem37 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem38 = New System.Windows.Forms.ToolStripMenuItem
        Me.SalesReturnToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator30 = New System.Windows.Forms.ToolStripSeparator
        Me.StockReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.StockByItemsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem8 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem12 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem13 = New System.Windows.Forms.ToolStripMenuItem
        Me.StockReportValueWiseClosingToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.StockReportValueWiseDetailToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.StocksByBrandToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem3 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem4 = New System.Windows.Forms.ToolStripMenuItem
        Me.StockReportsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.mnu_daily_stock = New System.Windows.Forms.ToolStripMenuItem
        Me.mnu_monthly_stock = New System.Windows.Forms.ToolStripMenuItem
        Me.mnu_d2d_stock = New System.Windows.Forms.ToolStripMenuItem
        Me.StockByMLToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem11 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem15 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem16 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator23 = New System.Windows.Forms.ToolStripSeparator
        Me.PurchaseReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.PurchaseReportsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.mnu_daily_purchase = New System.Windows.Forms.ToolStripMenuItem
        Me.mnu_monthly_purchase = New System.Windows.Forms.ToolStripMenuItem
        Me.mnu_d2d_purchase = New System.Windows.Forms.ToolStripMenuItem
        Me.PurchaseByBillNoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem14 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem18 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem19 = New System.Windows.Forms.ToolStripMenuItem
        Me.PurchaseSummaryByBillNoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem20 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem24 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem25 = New System.Windows.Forms.ToolStripMenuItem
        Me.PurchaseByItemToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem17 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem21 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem22 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator24 = New System.Windows.Forms.ToolStripSeparator
        Me.BreakageReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator13 = New System.Windows.Forms.ToolStripSeparator
        Me.ExciseReportsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator29 = New System.Windows.Forms.ToolStripSeparator
        Me.BulkLtrToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator32 = New System.Windows.Forms.ToolStripSeparator
        Me.AccountsReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.TransactionsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.CounterSaleToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator20 = New System.Windows.Forms.ToolStripSeparator
        Me.PurchaseBillToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator21 = New System.Windows.Forms.ToolStripSeparator
        Me.PaymentVoucherToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.PaymentVoucherToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator22 = New System.Windows.Forms.ToolStripSeparator
        Me.OpeningStockToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator25 = New System.Windows.Forms.ToolStripSeparator
        Me.StockTransferToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.BeakageEntryToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.TallyImportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ImportSalesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ImportPurchaseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.SETTINGSToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.CalculatorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator34 = New System.Windows.Forms.ToolStripSeparator
        Me.CREATESHORTCUTToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator35 = New System.Windows.Forms.ToolStripSeparator
        Me.CHANGEIDToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator36 = New System.Windows.Forms.ToolStripSeparator
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator37 = New System.Windows.Forms.ToolStripSeparator
        Me.BackUpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator33 = New System.Windows.Forms.ToolStripSeparator
        Me.DataFetchToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator31 = New System.Windows.Forms.ToolStripSeparator
        Me.SendSmsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.UpgradeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel
        Me.year_drop = New System.Windows.Forms.ToolStripDropDownButton
        Me.ToolStripStatusLabel6 = New System.Windows.Forms.ToolStripStatusLabel
        Me.ToolStripStatusLabel2 = New System.Windows.Forms.ToolStripStatusLabel
        Me.ToolStripStatusLabel3 = New System.Windows.Forms.ToolStripStatusLabel
        Me.ToolStripStatusLabel4 = New System.Windows.Forms.ToolStripStatusLabel
        Me.ToolStripStatusLabel5 = New System.Windows.Forms.ToolStripStatusLabel
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Button1 = New System.Windows.Forms.Button
        Me.ToolStrip2 = New System.Windows.Forms.ToolStrip
        Me.ts_companyinfo = New System.Windows.Forms.ToolStripLabel
        Me.ToolStripLabel5 = New System.Windows.Forms.ToolStripLabel
        Me.ToolStripLabel6 = New System.Windows.Forms.ToolStripLabel
        Me.ToolStripLabel7 = New System.Windows.Forms.ToolStripLabel
        Me.ToolStripLabel8 = New System.Windows.Forms.ToolStripLabel
        Me.ToolStripLabel9 = New System.Windows.Forms.ToolStripLabel
        Me.ToolStripLabel10 = New System.Windows.Forms.ToolStripLabel
        Me.ToolStripLabel11 = New System.Windows.Forms.ToolStripLabel
        Me.ToolStripLabel12 = New System.Windows.Forms.ToolStripLabel
        Me.ToolStripLabel13 = New System.Windows.Forms.ToolStripLabel
        Me.ToolStripLabel14 = New System.Windows.Forms.ToolStripLabel
        Me.ToolStripLabel15 = New System.Windows.Forms.ToolStripLabel
        Me.ToolStripLabel16 = New System.Windows.Forms.ToolStripLabel
        Me.ToolStripLabel17 = New System.Windows.Forms.ToolStripLabel
        Me.ToolStripLabel18 = New System.Windows.Forms.ToolStripLabel
        Me.ToolStripLabel19 = New System.Windows.Forms.ToolStripLabel
        Me.ToolStripLabel20 = New System.Windows.Forms.ToolStripLabel
        Me.ToolStripLabel21 = New System.Windows.Forms.ToolStripLabel
        Me.ToolStripLabel22 = New System.Windows.Forms.ToolStripLabel
        Me.ToolStripLabel23 = New System.Windows.Forms.ToolStripLabel
        Me.ToolStripSeparator12 = New System.Windows.Forms.ToolStripSeparator
        Me.mnu_closing_stock = New System.Windows.Forms.ToolStripLabel
        Me.ToolStripLabel2 = New System.Windows.Forms.ToolStripLabel
        Me.ToolStripSeparator14 = New System.Windows.Forms.ToolStripSeparator
        Me.ToolStripSeparator15 = New System.Windows.Forms.ToolStripSeparator
        Me.ToolStripLabel1 = New System.Windows.Forms.ToolStripLabel
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripButton
        Me.ToolStripButton3 = New System.Windows.Forms.ToolStripButton
        Me.MenuStrip1.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.ToolStrip2.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ContentsToolStripMenuItem
        '
        Me.ContentsToolStripMenuItem.Name = "ContentsToolStripMenuItem"
        Me.ContentsToolStripMenuItem.Size = New System.Drawing.Size(129, 22)
        Me.ContentsToolStripMenuItem.Text = "&Contents"
        '
        'IndexToolStripMenuItem
        '
        Me.IndexToolStripMenuItem.Name = "IndexToolStripMenuItem"
        Me.IndexToolStripMenuItem.Size = New System.Drawing.Size(129, 22)
        Me.IndexToolStripMenuItem.Text = "&Index"
        '
        'SearchToolStripMenuItem
        '
        Me.SearchToolStripMenuItem.Name = "SearchToolStripMenuItem"
        Me.SearchToolStripMenuItem.Size = New System.Drawing.Size(129, 22)
        Me.SearchToolStripMenuItem.Text = "&Search"
        '
        'toolStripSeparator5
        '
        Me.toolStripSeparator5.Name = "toolStripSeparator5"
        Me.toolStripSeparator5.Size = New System.Drawing.Size(126, 6)
        '
        'AboutToolStripMenuItem
        '
        Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
        Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(129, 22)
        Me.AboutToolStripMenuItem.Text = "&About..."
        '
        'NewToolStripMenuItem
        '
        Me.NewToolStripMenuItem.Image = CType(resources.GetObject("NewToolStripMenuItem.Image"), System.Drawing.Image)
        Me.NewToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.NewToolStripMenuItem.Name = "NewToolStripMenuItem"
        Me.NewToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N), System.Windows.Forms.Keys)
        Me.NewToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.NewToolStripMenuItem.Text = "&New"
        '
        'OpenToolStripMenuItem
        '
        Me.OpenToolStripMenuItem.Image = CType(resources.GetObject("OpenToolStripMenuItem.Image"), System.Drawing.Image)
        Me.OpenToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.OpenToolStripMenuItem.Name = "OpenToolStripMenuItem"
        Me.OpenToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.O), System.Windows.Forms.Keys)
        Me.OpenToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.OpenToolStripMenuItem.Text = "&Open"
        '
        'toolStripSeparator
        '
        Me.toolStripSeparator.Name = "toolStripSeparator"
        Me.toolStripSeparator.Size = New System.Drawing.Size(148, 6)
        '
        'SaveToolStripMenuItem
        '
        Me.SaveToolStripMenuItem.Image = CType(resources.GetObject("SaveToolStripMenuItem.Image"), System.Drawing.Image)
        Me.SaveToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.SaveToolStripMenuItem.Name = "SaveToolStripMenuItem"
        Me.SaveToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.SaveToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.SaveToolStripMenuItem.Text = "&Save"
        '
        'SaveAsToolStripMenuItem
        '
        Me.SaveAsToolStripMenuItem.Name = "SaveAsToolStripMenuItem"
        Me.SaveAsToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.SaveAsToolStripMenuItem.Text = "Save &As"
        '
        'toolStripSeparator1
        '
        Me.toolStripSeparator1.Name = "toolStripSeparator1"
        Me.toolStripSeparator1.Size = New System.Drawing.Size(148, 6)
        '
        'PrintToolStripMenuItem
        '
        Me.PrintToolStripMenuItem.Image = CType(resources.GetObject("PrintToolStripMenuItem.Image"), System.Drawing.Image)
        Me.PrintToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.PrintToolStripMenuItem.Name = "PrintToolStripMenuItem"
        Me.PrintToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.P), System.Windows.Forms.Keys)
        Me.PrintToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.PrintToolStripMenuItem.Text = "&Print"
        '
        'PrintPreviewToolStripMenuItem
        '
        Me.PrintPreviewToolStripMenuItem.Image = CType(resources.GetObject("PrintPreviewToolStripMenuItem.Image"), System.Drawing.Image)
        Me.PrintPreviewToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.PrintPreviewToolStripMenuItem.Name = "PrintPreviewToolStripMenuItem"
        Me.PrintPreviewToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.PrintPreviewToolStripMenuItem.Text = "Print Pre&view"
        '
        'toolStripSeparator2
        '
        Me.toolStripSeparator2.Name = "toolStripSeparator2"
        Me.toolStripSeparator2.Size = New System.Drawing.Size(148, 6)
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.ExitToolStripMenuItem.Text = "E&xit"
        '
        'UndoToolStripMenuItem
        '
        Me.UndoToolStripMenuItem.Name = "UndoToolStripMenuItem"
        Me.UndoToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Z), System.Windows.Forms.Keys)
        Me.UndoToolStripMenuItem.Size = New System.Drawing.Size(150, 22)
        Me.UndoToolStripMenuItem.Text = "&Undo"
        '
        'RedoToolStripMenuItem
        '
        Me.RedoToolStripMenuItem.Name = "RedoToolStripMenuItem"
        Me.RedoToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Y), System.Windows.Forms.Keys)
        Me.RedoToolStripMenuItem.Size = New System.Drawing.Size(150, 22)
        Me.RedoToolStripMenuItem.Text = "&Redo"
        '
        'toolStripSeparator3
        '
        Me.toolStripSeparator3.Name = "toolStripSeparator3"
        Me.toolStripSeparator3.Size = New System.Drawing.Size(147, 6)
        '
        'CutToolStripMenuItem
        '
        Me.CutToolStripMenuItem.Image = CType(resources.GetObject("CutToolStripMenuItem.Image"), System.Drawing.Image)
        Me.CutToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.CutToolStripMenuItem.Name = "CutToolStripMenuItem"
        Me.CutToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.X), System.Windows.Forms.Keys)
        Me.CutToolStripMenuItem.Size = New System.Drawing.Size(150, 22)
        Me.CutToolStripMenuItem.Text = "Cu&t"
        '
        'CopyToolStripMenuItem
        '
        Me.CopyToolStripMenuItem.Image = CType(resources.GetObject("CopyToolStripMenuItem.Image"), System.Drawing.Image)
        Me.CopyToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.CopyToolStripMenuItem.Name = "CopyToolStripMenuItem"
        Me.CopyToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.C), System.Windows.Forms.Keys)
        Me.CopyToolStripMenuItem.Size = New System.Drawing.Size(150, 22)
        Me.CopyToolStripMenuItem.Text = "&Copy"
        '
        'PasteToolStripMenuItem
        '
        Me.PasteToolStripMenuItem.Image = CType(resources.GetObject("PasteToolStripMenuItem.Image"), System.Drawing.Image)
        Me.PasteToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.PasteToolStripMenuItem.Name = "PasteToolStripMenuItem"
        Me.PasteToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.V), System.Windows.Forms.Keys)
        Me.PasteToolStripMenuItem.Size = New System.Drawing.Size(150, 22)
        Me.PasteToolStripMenuItem.Text = "&Paste"
        '
        'toolStripSeparator4
        '
        Me.toolStripSeparator4.Name = "toolStripSeparator4"
        Me.toolStripSeparator4.Size = New System.Drawing.Size(147, 6)
        '
        'SelectAllToolStripMenuItem
        '
        Me.SelectAllToolStripMenuItem.Name = "SelectAllToolStripMenuItem"
        Me.SelectAllToolStripMenuItem.Size = New System.Drawing.Size(150, 22)
        Me.SelectAllToolStripMenuItem.Text = "Select &All"
        '
        'CustomizeToolStripMenuItem
        '
        Me.CustomizeToolStripMenuItem.Name = "CustomizeToolStripMenuItem"
        Me.CustomizeToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me.CustomizeToolStripMenuItem.Text = "&Customize"
        '
        'OptionsToolStripMenuItem
        '
        Me.OptionsToolStripMenuItem.Name = "OptionsToolStripMenuItem"
        Me.OptionsToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me.OptionsToolStripMenuItem.Text = "&Options"
        '
        'NewToolStripMenuItem1
        '
        Me.NewToolStripMenuItem1.Image = CType(resources.GetObject("NewToolStripMenuItem1.Image"), System.Drawing.Image)
        Me.NewToolStripMenuItem1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.NewToolStripMenuItem1.Name = "NewToolStripMenuItem1"
        Me.NewToolStripMenuItem1.Size = New System.Drawing.Size(148, 22)
        Me.NewToolStripMenuItem1.Text = "&New"
        '
        'OpenToolStripMenuItem1
        '
        Me.OpenToolStripMenuItem1.Image = CType(resources.GetObject("OpenToolStripMenuItem1.Image"), System.Drawing.Image)
        Me.OpenToolStripMenuItem1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.OpenToolStripMenuItem1.Name = "OpenToolStripMenuItem1"
        Me.OpenToolStripMenuItem1.Size = New System.Drawing.Size(148, 22)
        Me.OpenToolStripMenuItem1.Text = "&Open"
        '
        'toolStripSeparator6
        '
        Me.toolStripSeparator6.Name = "toolStripSeparator6"
        Me.toolStripSeparator6.Size = New System.Drawing.Size(145, 6)
        '
        'SaveToolStripMenuItem1
        '
        Me.SaveToolStripMenuItem1.Image = CType(resources.GetObject("SaveToolStripMenuItem1.Image"), System.Drawing.Image)
        Me.SaveToolStripMenuItem1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.SaveToolStripMenuItem1.Name = "SaveToolStripMenuItem1"
        Me.SaveToolStripMenuItem1.Size = New System.Drawing.Size(148, 22)
        Me.SaveToolStripMenuItem1.Text = "&Save"
        '
        'SaveAsToolStripMenuItem1
        '
        Me.SaveAsToolStripMenuItem1.Name = "SaveAsToolStripMenuItem1"
        Me.SaveAsToolStripMenuItem1.Size = New System.Drawing.Size(148, 22)
        Me.SaveAsToolStripMenuItem1.Text = "Save &As"
        '
        'toolStripSeparator7
        '
        Me.toolStripSeparator7.Name = "toolStripSeparator7"
        Me.toolStripSeparator7.Size = New System.Drawing.Size(145, 6)
        '
        'PrintToolStripMenuItem1
        '
        Me.PrintToolStripMenuItem1.Image = CType(resources.GetObject("PrintToolStripMenuItem1.Image"), System.Drawing.Image)
        Me.PrintToolStripMenuItem1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.PrintToolStripMenuItem1.Name = "PrintToolStripMenuItem1"
        Me.PrintToolStripMenuItem1.Size = New System.Drawing.Size(148, 22)
        Me.PrintToolStripMenuItem1.Text = "&Print"
        '
        'PrintPreviewToolStripMenuItem1
        '
        Me.PrintPreviewToolStripMenuItem1.Image = CType(resources.GetObject("PrintPreviewToolStripMenuItem1.Image"), System.Drawing.Image)
        Me.PrintPreviewToolStripMenuItem1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.PrintPreviewToolStripMenuItem1.Name = "PrintPreviewToolStripMenuItem1"
        Me.PrintPreviewToolStripMenuItem1.Size = New System.Drawing.Size(148, 22)
        Me.PrintPreviewToolStripMenuItem1.Text = "Print Pre&view"
        '
        'toolStripSeparator8
        '
        Me.toolStripSeparator8.Name = "toolStripSeparator8"
        Me.toolStripSeparator8.Size = New System.Drawing.Size(145, 6)
        '
        'ExitToolStripMenuItem1
        '
        Me.ExitToolStripMenuItem1.Name = "ExitToolStripMenuItem1"
        Me.ExitToolStripMenuItem1.Size = New System.Drawing.Size(148, 22)
        Me.ExitToolStripMenuItem1.Text = "E&xit"
        '
        'UndoToolStripMenuItem1
        '
        Me.UndoToolStripMenuItem1.Name = "UndoToolStripMenuItem1"
        Me.UndoToolStripMenuItem1.Size = New System.Drawing.Size(128, 22)
        Me.UndoToolStripMenuItem1.Text = "&Undo"
        '
        'RedoToolStripMenuItem1
        '
        Me.RedoToolStripMenuItem1.Name = "RedoToolStripMenuItem1"
        Me.RedoToolStripMenuItem1.Size = New System.Drawing.Size(128, 22)
        Me.RedoToolStripMenuItem1.Text = "&Redo"
        '
        'toolStripSeparator9
        '
        Me.toolStripSeparator9.Name = "toolStripSeparator9"
        Me.toolStripSeparator9.Size = New System.Drawing.Size(125, 6)
        '
        'CutToolStripMenuItem1
        '
        Me.CutToolStripMenuItem1.Image = CType(resources.GetObject("CutToolStripMenuItem1.Image"), System.Drawing.Image)
        Me.CutToolStripMenuItem1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.CutToolStripMenuItem1.Name = "CutToolStripMenuItem1"
        Me.CutToolStripMenuItem1.Size = New System.Drawing.Size(128, 22)
        Me.CutToolStripMenuItem1.Text = "Cu&t"
        '
        'CopyToolStripMenuItem1
        '
        Me.CopyToolStripMenuItem1.Image = CType(resources.GetObject("CopyToolStripMenuItem1.Image"), System.Drawing.Image)
        Me.CopyToolStripMenuItem1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.CopyToolStripMenuItem1.Name = "CopyToolStripMenuItem1"
        Me.CopyToolStripMenuItem1.Size = New System.Drawing.Size(128, 22)
        Me.CopyToolStripMenuItem1.Text = "&Copy"
        '
        'PasteToolStripMenuItem1
        '
        Me.PasteToolStripMenuItem1.Image = CType(resources.GetObject("PasteToolStripMenuItem1.Image"), System.Drawing.Image)
        Me.PasteToolStripMenuItem1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.PasteToolStripMenuItem1.Name = "PasteToolStripMenuItem1"
        Me.PasteToolStripMenuItem1.Size = New System.Drawing.Size(128, 22)
        Me.PasteToolStripMenuItem1.Text = "&Paste"
        '
        'toolStripSeparator10
        '
        Me.toolStripSeparator10.Name = "toolStripSeparator10"
        Me.toolStripSeparator10.Size = New System.Drawing.Size(125, 6)
        '
        'SelectAllToolStripMenuItem1
        '
        Me.SelectAllToolStripMenuItem1.Name = "SelectAllToolStripMenuItem1"
        Me.SelectAllToolStripMenuItem1.Size = New System.Drawing.Size(128, 22)
        Me.SelectAllToolStripMenuItem1.Text = "Select &All"
        '
        'CustomizeToolStripMenuItem1
        '
        Me.CustomizeToolStripMenuItem1.Name = "CustomizeToolStripMenuItem1"
        Me.CustomizeToolStripMenuItem1.Size = New System.Drawing.Size(134, 22)
        Me.CustomizeToolStripMenuItem1.Text = "&Customize"
        '
        'OptionsToolStripMenuItem1
        '
        Me.OptionsToolStripMenuItem1.Name = "OptionsToolStripMenuItem1"
        Me.OptionsToolStripMenuItem1.Size = New System.Drawing.Size(134, 22)
        Me.OptionsToolStripMenuItem1.Text = "&Options"
        '
        'ContentsToolStripMenuItem1
        '
        Me.ContentsToolStripMenuItem1.Name = "ContentsToolStripMenuItem1"
        Me.ContentsToolStripMenuItem1.Size = New System.Drawing.Size(129, 22)
        Me.ContentsToolStripMenuItem1.Text = "&Contents"
        '
        'IndexToolStripMenuItem1
        '
        Me.IndexToolStripMenuItem1.Name = "IndexToolStripMenuItem1"
        Me.IndexToolStripMenuItem1.Size = New System.Drawing.Size(129, 22)
        Me.IndexToolStripMenuItem1.Text = "&Index"
        '
        'SearchToolStripMenuItem1
        '
        Me.SearchToolStripMenuItem1.Name = "SearchToolStripMenuItem1"
        Me.SearchToolStripMenuItem1.Size = New System.Drawing.Size(129, 22)
        Me.SearchToolStripMenuItem1.Text = "&Search"
        '
        'toolStripSeparator11
        '
        Me.toolStripSeparator11.Name = "toolStripSeparator11"
        Me.toolStripSeparator11.Size = New System.Drawing.Size(126, 6)
        '
        'AboutToolStripMenuItem1
        '
        Me.AboutToolStripMenuItem1.Name = "AboutToolStripMenuItem1"
        Me.AboutToolStripMenuItem1.Size = New System.Drawing.Size(129, 22)
        Me.AboutToolStripMenuItem1.Text = "&About..."
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.Color.NavajoWhite
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MainToolStripMenuItem, Me.FILEToolStripMenuItem, Me.AdminToolStripMenuItem, Me.MasterToolStripMenuItem, Me.TransactionsToolStripMenuItem, Me.REPORTToolStripMenuItem, Me.TallyImportToolStripMenuItem, Me.SETTINGSToolStripMenuItem, Me.UpgradeToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1018, 24)
        Me.MenuStrip1.TabIndex = 2
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'MainToolStripMenuItem
        '
        Me.MainToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LogToolStripMenuItem, Me.LogOutToolStripMenuItem, Me.ToolStripSeparator26, Me.ChangeServerToolStripMenuItem, Me.ChangeCompanyToolStripMenuItem, Me.ToolStripSeparator27, Me.ToolStripSeparator28, Me.ExitToolStripMenuItem2})
        Me.MainToolStripMenuItem.Name = "MainToolStripMenuItem"
        Me.MainToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.A), System.Windows.Forms.Keys)
        Me.MainToolStripMenuItem.Size = New System.Drawing.Size(46, 20)
        Me.MainToolStripMenuItem.Text = "Main"
        '
        'LogToolStripMenuItem
        '
        Me.LogToolStripMenuItem.Name = "LogToolStripMenuItem"
        Me.LogToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5
        Me.LogToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.LogToolStripMenuItem.Text = "Log In"
        '
        'LogOutToolStripMenuItem
        '
        Me.LogOutToolStripMenuItem.Name = "LogOutToolStripMenuItem"
        Me.LogOutToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F6
        Me.LogOutToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.LogOutToolStripMenuItem.Text = "Log &Out"
        '
        'ToolStripSeparator26
        '
        Me.ToolStripSeparator26.Name = "ToolStripSeparator26"
        Me.ToolStripSeparator26.Size = New System.Drawing.Size(186, 6)
        '
        'ChangeServerToolStripMenuItem
        '
        Me.ChangeServerToolStripMenuItem.Name = "ChangeServerToolStripMenuItem"
        Me.ChangeServerToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.ChangeServerToolStripMenuItem.Text = "Change Server"
        '
        'ChangeCompanyToolStripMenuItem
        '
        Me.ChangeCompanyToolStripMenuItem.Name = "ChangeCompanyToolStripMenuItem"
        Me.ChangeCompanyToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F7
        Me.ChangeCompanyToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.ChangeCompanyToolStripMenuItem.Text = "Change Company"
        '
        'ToolStripSeparator27
        '
        Me.ToolStripSeparator27.Name = "ToolStripSeparator27"
        Me.ToolStripSeparator27.Size = New System.Drawing.Size(186, 6)
        '
        'ToolStripSeparator28
        '
        Me.ToolStripSeparator28.Name = "ToolStripSeparator28"
        Me.ToolStripSeparator28.Size = New System.Drawing.Size(186, 6)
        '
        'ExitToolStripMenuItem2
        '
        Me.ExitToolStripMenuItem2.Name = "ExitToolStripMenuItem2"
        Me.ExitToolStripMenuItem2.ShortcutKeys = System.Windows.Forms.Keys.F8
        Me.ExitToolStripMenuItem2.Size = New System.Drawing.Size(189, 22)
        Me.ExitToolStripMenuItem2.Text = "E&xit"
        '
        'FILEToolStripMenuItem
        '
        Me.FILEToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GETEXCELLToolStripMenuItem, Me.GETWORDDOCUMENTToolStripMenuItem, Me.GETTXTBYTRNNOToolStripMenuItem, Me.GETTXTBYDATEToolStripMenuItem})
        Me.FILEToolStripMenuItem.Name = "FILEToolStripMenuItem"
        Me.FILEToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FILEToolStripMenuItem.Text = "&File"
        Me.FILEToolStripMenuItem.Visible = False
        '
        'GETEXCELLToolStripMenuItem
        '
        Me.GETEXCELLToolStripMenuItem.Name = "GETEXCELLToolStripMenuItem"
        Me.GETEXCELLToolStripMenuItem.Size = New System.Drawing.Size(202, 22)
        Me.GETEXCELLToolStripMenuItem.Text = "GET EXCELL"
        '
        'GETWORDDOCUMENTToolStripMenuItem
        '
        Me.GETWORDDOCUMENTToolStripMenuItem.Name = "GETWORDDOCUMENTToolStripMenuItem"
        Me.GETWORDDOCUMENTToolStripMenuItem.Size = New System.Drawing.Size(202, 22)
        Me.GETWORDDOCUMENTToolStripMenuItem.Text = "GET WORD DOCUMENT"
        '
        'GETTXTBYTRNNOToolStripMenuItem
        '
        Me.GETTXTBYTRNNOToolStripMenuItem.Name = "GETTXTBYTRNNOToolStripMenuItem"
        Me.GETTXTBYTRNNOToolStripMenuItem.Size = New System.Drawing.Size(202, 22)
        Me.GETTXTBYTRNNOToolStripMenuItem.Text = "GET TXT BY TRN NO"
        '
        'GETTXTBYDATEToolStripMenuItem
        '
        Me.GETTXTBYDATEToolStripMenuItem.Name = "GETTXTBYDATEToolStripMenuItem"
        Me.GETTXTBYDATEToolStripMenuItem.Size = New System.Drawing.Size(202, 22)
        Me.GETTXTBYDATEToolStripMenuItem.Text = "GET TXT BY DATE"
        '
        'AdminToolStripMenuItem
        '
        Me.AdminToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CREATECOMPANYToolStripMenuItem, Me.CompanyParameterToolStripMenuItem, Me.ToolStripSeparator18, Me.UserInformationToolStripMenuItem, Me.UserRightsToolStripMenuItem, Me.ToolStripSeparator19, Me.NeToolStripMenuItem})
        Me.AdminToolStripMenuItem.Name = "AdminToolStripMenuItem"
        Me.AdminToolStripMenuItem.Size = New System.Drawing.Size(55, 20)
        Me.AdminToolStripMenuItem.Text = "&Admin"
        '
        'CREATECOMPANYToolStripMenuItem
        '
        Me.CREATECOMPANYToolStripMenuItem.Name = "CREATECOMPANYToolStripMenuItem"
        Me.CREATECOMPANYToolStripMenuItem.Size = New System.Drawing.Size(192, 22)
        Me.CREATECOMPANYToolStripMenuItem.Text = "Company Information"
        '
        'CompanyParameterToolStripMenuItem
        '
        Me.CompanyParameterToolStripMenuItem.Name = "CompanyParameterToolStripMenuItem"
        Me.CompanyParameterToolStripMenuItem.Size = New System.Drawing.Size(192, 22)
        Me.CompanyParameterToolStripMenuItem.Text = "Company Parameter"
        '
        'ToolStripSeparator18
        '
        Me.ToolStripSeparator18.Name = "ToolStripSeparator18"
        Me.ToolStripSeparator18.Size = New System.Drawing.Size(189, 6)
        '
        'UserInformationToolStripMenuItem
        '
        Me.UserInformationToolStripMenuItem.Name = "UserInformationToolStripMenuItem"
        Me.UserInformationToolStripMenuItem.Size = New System.Drawing.Size(192, 22)
        Me.UserInformationToolStripMenuItem.Text = "User Information"
        '
        'UserRightsToolStripMenuItem
        '
        Me.UserRightsToolStripMenuItem.Name = "UserRightsToolStripMenuItem"
        Me.UserRightsToolStripMenuItem.Size = New System.Drawing.Size(192, 22)
        Me.UserRightsToolStripMenuItem.Text = "User Rights"
        '
        'ToolStripSeparator19
        '
        Me.ToolStripSeparator19.Name = "ToolStripSeparator19"
        Me.ToolStripSeparator19.Size = New System.Drawing.Size(189, 6)
        '
        'NeToolStripMenuItem
        '
        Me.NeToolStripMenuItem.Name = "NeToolStripMenuItem"
        Me.NeToolStripMenuItem.Size = New System.Drawing.Size(192, 22)
        Me.NeToolStripMenuItem.Text = "New Year Creation"
        '
        'MasterToolStripMenuItem
        '
        Me.MasterToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AccountCreationToolStripMenuItem, Me.AcountSubGroupToolStripMenuItem, Me.LedgerToolStripMenuItem, Me.TaxShemesToolStripMenuItem, Me.ToolStripSeparator16, Me.BrandMasterToolStripMenuItem, Me.ItemCateToolStripMenuItem, Me.KindOfForeignLoquoToolStripMenuItem, Me.MeasurePackingToolStripMenuItem, Me.StrengthToolStripMenuItem, Me.ItemMasterToolStripMenuItem, Me.SalesRateToolStripMenuItem, Me.ToolStripSeparator17, Me.StorageLocationToolStripMenuItem})
        Me.MasterToolStripMenuItem.Name = "MasterToolStripMenuItem"
        Me.MasterToolStripMenuItem.Size = New System.Drawing.Size(60, 20)
        Me.MasterToolStripMenuItem.Text = "&Masters"
        '
        'AccountCreationToolStripMenuItem
        '
        Me.AccountCreationToolStripMenuItem.Name = "AccountCreationToolStripMenuItem"
        Me.AccountCreationToolStripMenuItem.Size = New System.Drawing.Size(192, 22)
        Me.AccountCreationToolStripMenuItem.Text = "Account Main Group"
        '
        'AcountSubGroupToolStripMenuItem
        '
        Me.AcountSubGroupToolStripMenuItem.Name = "AcountSubGroupToolStripMenuItem"
        Me.AcountSubGroupToolStripMenuItem.Size = New System.Drawing.Size(192, 22)
        Me.AcountSubGroupToolStripMenuItem.Text = "Account Sub Group"
        '
        'LedgerToolStripMenuItem
        '
        Me.LedgerToolStripMenuItem.Name = "LedgerToolStripMenuItem"
        Me.LedgerToolStripMenuItem.Size = New System.Drawing.Size(192, 22)
        Me.LedgerToolStripMenuItem.Text = "Ledger"
        '
        'TaxShemesToolStripMenuItem
        '
        Me.TaxShemesToolStripMenuItem.Name = "TaxShemesToolStripMenuItem"
        Me.TaxShemesToolStripMenuItem.Size = New System.Drawing.Size(192, 22)
        Me.TaxShemesToolStripMenuItem.Text = "Tax Shemes"
        '
        'ToolStripSeparator16
        '
        Me.ToolStripSeparator16.Name = "ToolStripSeparator16"
        Me.ToolStripSeparator16.Size = New System.Drawing.Size(189, 6)
        '
        'BrandMasterToolStripMenuItem
        '
        Me.BrandMasterToolStripMenuItem.Name = "BrandMasterToolStripMenuItem"
        Me.BrandMasterToolStripMenuItem.Size = New System.Drawing.Size(192, 22)
        Me.BrandMasterToolStripMenuItem.Text = "Brand Master"
        '
        'ItemCateToolStripMenuItem
        '
        Me.ItemCateToolStripMenuItem.Name = "ItemCateToolStripMenuItem"
        Me.ItemCateToolStripMenuItem.Size = New System.Drawing.Size(192, 22)
        Me.ItemCateToolStripMenuItem.Text = "Category Master"
        '
        'KindOfForeignLoquoToolStripMenuItem
        '
        Me.KindOfForeignLoquoToolStripMenuItem.Name = "KindOfForeignLoquoToolStripMenuItem"
        Me.KindOfForeignLoquoToolStripMenuItem.Size = New System.Drawing.Size(192, 22)
        Me.KindOfForeignLoquoToolStripMenuItem.Text = "Kind of Foreign Liquor"
        '
        'MeasurePackingToolStripMenuItem
        '
        Me.MeasurePackingToolStripMenuItem.Name = "MeasurePackingToolStripMenuItem"
        Me.MeasurePackingToolStripMenuItem.Size = New System.Drawing.Size(192, 22)
        Me.MeasurePackingToolStripMenuItem.Text = "Measure and Packing"
        '
        'StrengthToolStripMenuItem
        '
        Me.StrengthToolStripMenuItem.Name = "StrengthToolStripMenuItem"
        Me.StrengthToolStripMenuItem.Size = New System.Drawing.Size(192, 22)
        Me.StrengthToolStripMenuItem.Text = "Strength Master"
        '
        'ItemMasterToolStripMenuItem
        '
        Me.ItemMasterToolStripMenuItem.Name = "ItemMasterToolStripMenuItem"
        Me.ItemMasterToolStripMenuItem.Size = New System.Drawing.Size(192, 22)
        Me.ItemMasterToolStripMenuItem.Text = "Item Information"
        '
        'SalesRateToolStripMenuItem
        '
        Me.SalesRateToolStripMenuItem.Name = "SalesRateToolStripMenuItem"
        Me.SalesRateToolStripMenuItem.Size = New System.Drawing.Size(192, 22)
        Me.SalesRateToolStripMenuItem.Text = "Sales Rate"
        '
        'ToolStripSeparator17
        '
        Me.ToolStripSeparator17.Name = "ToolStripSeparator17"
        Me.ToolStripSeparator17.Size = New System.Drawing.Size(189, 6)
        '
        'StorageLocationToolStripMenuItem
        '
        Me.StorageLocationToolStripMenuItem.Name = "StorageLocationToolStripMenuItem"
        Me.StorageLocationToolStripMenuItem.Size = New System.Drawing.Size(192, 22)
        Me.StorageLocationToolStripMenuItem.Text = "Storage Location"
        '
        'REPORTToolStripMenuItem
        '
        Me.REPORTToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SalesReportToolStripMenuItem, Me.ToolStripSeparator30, Me.StockReportToolStripMenuItem, Me.ToolStripSeparator23, Me.PurchaseReportToolStripMenuItem, Me.ToolStripSeparator24, Me.BreakageReportToolStripMenuItem, Me.ToolStripSeparator13, Me.ExciseReportsToolStripMenuItem, Me.ToolStripSeparator29, Me.BulkLtrToolStripMenuItem, Me.ToolStripSeparator32, Me.AccountsReportToolStripMenuItem})
        Me.REPORTToolStripMenuItem.Name = "REPORTToolStripMenuItem"
        Me.REPORTToolStripMenuItem.Size = New System.Drawing.Size(54, 20)
        Me.REPORTToolStripMenuItem.Text = "&Report"
        '
        'SalesReportToolStripMenuItem
        '
        Me.SalesReportToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SaleReportsToolStripMenuItem, Me.mnu_multybill_print, Me.ToolStripMenuItem26, Me.ToolStripMenuItem27, Me.ToolStripMenuItem23, Me.ToolStripMenuItem31, Me.ToolStripMenuItem35, Me.SalesReturnToolStripMenuItem})
        Me.SalesReportToolStripMenuItem.Name = "SalesReportToolStripMenuItem"
        Me.SalesReportToolStripMenuItem.Size = New System.Drawing.Size(162, 22)
        Me.SalesReportToolStripMenuItem.Text = "Sales Report"
        '
        'SaleReportsToolStripMenuItem
        '
        Me.SaleReportsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnu_daily_sale, Me.mnu_monthly_sale, Me.mnu_d2d_sale})
        Me.SaleReportsToolStripMenuItem.Name = "SaleReportsToolStripMenuItem"
        Me.SaleReportsToolStripMenuItem.Size = New System.Drawing.Size(229, 22)
        Me.SaleReportsToolStripMenuItem.Text = "Sales Statement"
        '
        'mnu_daily_sale
        '
        Me.mnu_daily_sale.Name = "mnu_daily_sale"
        Me.mnu_daily_sale.Size = New System.Drawing.Size(160, 22)
        Me.mnu_daily_sale.Text = "Daily Sales"
        Me.mnu_daily_sale.Visible = False
        '
        'mnu_monthly_sale
        '
        Me.mnu_monthly_sale.Name = "mnu_monthly_sale"
        Me.mnu_monthly_sale.Size = New System.Drawing.Size(160, 22)
        Me.mnu_monthly_sale.Text = "Monthly Sales"
        Me.mnu_monthly_sale.Visible = False
        '
        'mnu_d2d_sale
        '
        Me.mnu_d2d_sale.Name = "mnu_d2d_sale"
        Me.mnu_d2d_sale.Size = New System.Drawing.Size(160, 22)
        Me.mnu_d2d_sale.Text = "Day to Day Sales"
        Me.mnu_d2d_sale.Visible = False
        '
        'mnu_multybill_print
        '
        Me.mnu_multybill_print.Name = "mnu_multybill_print"
        Me.mnu_multybill_print.Size = New System.Drawing.Size(229, 22)
        Me.mnu_multybill_print.Text = "Multy Bill Print"
        Me.mnu_multybill_print.Visible = False
        '
        'ToolStripMenuItem26
        '
        Me.ToolStripMenuItem26.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem42, Me.ToolStripMenuItem43, Me.ToolStripMenuItem44})
        Me.ToolStripMenuItem26.Name = "ToolStripMenuItem26"
        Me.ToolStripMenuItem26.Size = New System.Drawing.Size(229, 22)
        Me.ToolStripMenuItem26.Text = "Sales Reports - ML Wilse"
        '
        'ToolStripMenuItem42
        '
        Me.ToolStripMenuItem42.Name = "ToolStripMenuItem42"
        Me.ToolStripMenuItem42.Size = New System.Drawing.Size(160, 22)
        Me.ToolStripMenuItem42.Text = "Daily Sales"
        Me.ToolStripMenuItem42.Visible = False
        '
        'ToolStripMenuItem43
        '
        Me.ToolStripMenuItem43.Name = "ToolStripMenuItem43"
        Me.ToolStripMenuItem43.Size = New System.Drawing.Size(160, 22)
        Me.ToolStripMenuItem43.Text = "Monthly Sales"
        Me.ToolStripMenuItem43.Visible = False
        '
        'ToolStripMenuItem44
        '
        Me.ToolStripMenuItem44.Name = "ToolStripMenuItem44"
        Me.ToolStripMenuItem44.Size = New System.Drawing.Size(160, 22)
        Me.ToolStripMenuItem44.Text = "Day to Day Sales"
        Me.ToolStripMenuItem44.Visible = False
        '
        'ToolStripMenuItem27
        '
        Me.ToolStripMenuItem27.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem28, Me.ToolStripMenuItem29, Me.ToolStripMenuItem30})
        Me.ToolStripMenuItem27.Name = "ToolStripMenuItem27"
        Me.ToolStripMenuItem27.Size = New System.Drawing.Size(229, 22)
        Me.ToolStripMenuItem27.Text = "Sales Reports - Amount Wilse"
        Me.ToolStripMenuItem27.Visible = False
        '
        'ToolStripMenuItem28
        '
        Me.ToolStripMenuItem28.Name = "ToolStripMenuItem28"
        Me.ToolStripMenuItem28.Size = New System.Drawing.Size(160, 22)
        Me.ToolStripMenuItem28.Text = "Daily Sales"
        Me.ToolStripMenuItem28.Visible = False
        '
        'ToolStripMenuItem29
        '
        Me.ToolStripMenuItem29.Name = "ToolStripMenuItem29"
        Me.ToolStripMenuItem29.Size = New System.Drawing.Size(160, 22)
        Me.ToolStripMenuItem29.Text = "Monthly Sales"
        Me.ToolStripMenuItem29.Visible = False
        '
        'ToolStripMenuItem30
        '
        Me.ToolStripMenuItem30.Name = "ToolStripMenuItem30"
        Me.ToolStripMenuItem30.Size = New System.Drawing.Size(160, 22)
        Me.ToolStripMenuItem30.Text = "Day to Day Sales"
        Me.ToolStripMenuItem30.Visible = False
        '
        'ToolStripMenuItem23
        '
        Me.ToolStripMenuItem23.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem39, Me.ToolStripMenuItem40, Me.ToolStripMenuItem41})
        Me.ToolStripMenuItem23.Name = "ToolStripMenuItem23"
        Me.ToolStripMenuItem23.Size = New System.Drawing.Size(229, 22)
        Me.ToolStripMenuItem23.Text = "Sales Reports - Party Wilse"
        '
        'ToolStripMenuItem39
        '
        Me.ToolStripMenuItem39.Name = "ToolStripMenuItem39"
        Me.ToolStripMenuItem39.Size = New System.Drawing.Size(160, 22)
        Me.ToolStripMenuItem39.Text = "Daily Sales"
        Me.ToolStripMenuItem39.Visible = False
        '
        'ToolStripMenuItem40
        '
        Me.ToolStripMenuItem40.Name = "ToolStripMenuItem40"
        Me.ToolStripMenuItem40.Size = New System.Drawing.Size(160, 22)
        Me.ToolStripMenuItem40.Text = "Monthly Sales"
        Me.ToolStripMenuItem40.Visible = False
        '
        'ToolStripMenuItem41
        '
        Me.ToolStripMenuItem41.Name = "ToolStripMenuItem41"
        Me.ToolStripMenuItem41.Size = New System.Drawing.Size(160, 22)
        Me.ToolStripMenuItem41.Text = "Day to Day Sales"
        Me.ToolStripMenuItem41.Visible = False
        '
        'ToolStripMenuItem31
        '
        Me.ToolStripMenuItem31.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem32, Me.ToolStripMenuItem33, Me.ToolStripMenuItem34})
        Me.ToolStripMenuItem31.Name = "ToolStripMenuItem31"
        Me.ToolStripMenuItem31.Size = New System.Drawing.Size(229, 22)
        Me.ToolStripMenuItem31.Text = "Sales Reports - Bill Wilse"
        '
        'ToolStripMenuItem32
        '
        Me.ToolStripMenuItem32.Name = "ToolStripMenuItem32"
        Me.ToolStripMenuItem32.Size = New System.Drawing.Size(160, 22)
        Me.ToolStripMenuItem32.Text = "Daily Sales"
        Me.ToolStripMenuItem32.Visible = False
        '
        'ToolStripMenuItem33
        '
        Me.ToolStripMenuItem33.Name = "ToolStripMenuItem33"
        Me.ToolStripMenuItem33.Size = New System.Drawing.Size(160, 22)
        Me.ToolStripMenuItem33.Text = "Monthly Sales"
        Me.ToolStripMenuItem33.Visible = False
        '
        'ToolStripMenuItem34
        '
        Me.ToolStripMenuItem34.Name = "ToolStripMenuItem34"
        Me.ToolStripMenuItem34.Size = New System.Drawing.Size(160, 22)
        Me.ToolStripMenuItem34.Text = "Day to Day Sales"
        Me.ToolStripMenuItem34.Visible = False
        '
        'ToolStripMenuItem35
        '
        Me.ToolStripMenuItem35.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem36, Me.ToolStripMenuItem37, Me.ToolStripMenuItem38})
        Me.ToolStripMenuItem35.Name = "ToolStripMenuItem35"
        Me.ToolStripMenuItem35.Size = New System.Drawing.Size(229, 22)
        Me.ToolStripMenuItem35.Text = "Sales Report - Item Wise"
        '
        'ToolStripMenuItem36
        '
        Me.ToolStripMenuItem36.Name = "ToolStripMenuItem36"
        Me.ToolStripMenuItem36.Size = New System.Drawing.Size(160, 22)
        Me.ToolStripMenuItem36.Text = "Daily Sales"
        Me.ToolStripMenuItem36.Visible = False
        '
        'ToolStripMenuItem37
        '
        Me.ToolStripMenuItem37.Name = "ToolStripMenuItem37"
        Me.ToolStripMenuItem37.Size = New System.Drawing.Size(160, 22)
        Me.ToolStripMenuItem37.Text = "Monthly Sales"
        Me.ToolStripMenuItem37.Visible = False
        '
        'ToolStripMenuItem38
        '
        Me.ToolStripMenuItem38.Name = "ToolStripMenuItem38"
        Me.ToolStripMenuItem38.Size = New System.Drawing.Size(160, 22)
        Me.ToolStripMenuItem38.Text = "Day to Day Sales"
        Me.ToolStripMenuItem38.Visible = False
        '
        'SalesReturnToolStripMenuItem
        '
        Me.SalesReturnToolStripMenuItem.Name = "SalesReturnToolStripMenuItem"
        Me.SalesReturnToolStripMenuItem.Size = New System.Drawing.Size(229, 22)
        Me.SalesReturnToolStripMenuItem.Text = "Sales Return"
        '
        'ToolStripSeparator30
        '
        Me.ToolStripSeparator30.Name = "ToolStripSeparator30"
        Me.ToolStripSeparator30.Size = New System.Drawing.Size(159, 6)
        '
        'StockReportToolStripMenuItem
        '
        Me.StockReportToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.StockByItemsToolStripMenuItem, Me.StockReportValueWiseClosingToolStripMenuItem, Me.StockReportValueWiseDetailToolStripMenuItem, Me.StocksByBrandToolStripMenuItem, Me.StockReportsToolStripMenuItem, Me.StockByMLToolStripMenuItem})
        Me.StockReportToolStripMenuItem.Name = "StockReportToolStripMenuItem"
        Me.StockReportToolStripMenuItem.Size = New System.Drawing.Size(162, 22)
        Me.StockReportToolStripMenuItem.Text = "Stock Report"
        '
        'StockByItemsToolStripMenuItem
        '
        Me.StockByItemsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem8, Me.ToolStripMenuItem12, Me.ToolStripMenuItem13})
        Me.StockByItemsToolStripMenuItem.Name = "StockByItemsToolStripMenuItem"
        Me.StockByItemsToolStripMenuItem.Size = New System.Drawing.Size(252, 22)
        Me.StockByItemsToolStripMenuItem.Text = "Stock Report - Items Wise Closing"
        '
        'ToolStripMenuItem8
        '
        Me.ToolStripMenuItem8.Name = "ToolStripMenuItem8"
        Me.ToolStripMenuItem8.Size = New System.Drawing.Size(163, 22)
        Me.ToolStripMenuItem8.Text = "Daily Stock"
        Me.ToolStripMenuItem8.Visible = False
        '
        'ToolStripMenuItem12
        '
        Me.ToolStripMenuItem12.Name = "ToolStripMenuItem12"
        Me.ToolStripMenuItem12.Size = New System.Drawing.Size(163, 22)
        Me.ToolStripMenuItem12.Text = "Monthly Stock"
        Me.ToolStripMenuItem12.Visible = False
        '
        'ToolStripMenuItem13
        '
        Me.ToolStripMenuItem13.Name = "ToolStripMenuItem13"
        Me.ToolStripMenuItem13.Size = New System.Drawing.Size(163, 22)
        Me.ToolStripMenuItem13.Text = "Day to Day Srock"
        Me.ToolStripMenuItem13.Visible = False
        '
        'StockReportValueWiseClosingToolStripMenuItem
        '
        Me.StockReportValueWiseClosingToolStripMenuItem.Name = "StockReportValueWiseClosingToolStripMenuItem"
        Me.StockReportValueWiseClosingToolStripMenuItem.Size = New System.Drawing.Size(252, 22)
        Me.StockReportValueWiseClosingToolStripMenuItem.Text = "Stock Report - Value Wise Closing"
        '
        'StockReportValueWiseDetailToolStripMenuItem
        '
        Me.StockReportValueWiseDetailToolStripMenuItem.Name = "StockReportValueWiseDetailToolStripMenuItem"
        Me.StockReportValueWiseDetailToolStripMenuItem.Size = New System.Drawing.Size(252, 22)
        Me.StockReportValueWiseDetailToolStripMenuItem.Text = "Stock Report - Value Wise Detail"
        '
        'StocksByBrandToolStripMenuItem
        '
        Me.StocksByBrandToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem1, Me.ToolStripMenuItem3, Me.ToolStripMenuItem4})
        Me.StocksByBrandToolStripMenuItem.Name = "StocksByBrandToolStripMenuItem"
        Me.StocksByBrandToolStripMenuItem.Size = New System.Drawing.Size(252, 22)
        Me.StocksByBrandToolStripMenuItem.Text = "Stock Report - Brand Wise"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(163, 22)
        Me.ToolStripMenuItem1.Text = "Daily Stock"
        Me.ToolStripMenuItem1.Visible = False
        '
        'ToolStripMenuItem3
        '
        Me.ToolStripMenuItem3.Name = "ToolStripMenuItem3"
        Me.ToolStripMenuItem3.Size = New System.Drawing.Size(163, 22)
        Me.ToolStripMenuItem3.Text = "Monthly Stock"
        Me.ToolStripMenuItem3.Visible = False
        '
        'ToolStripMenuItem4
        '
        Me.ToolStripMenuItem4.Name = "ToolStripMenuItem4"
        Me.ToolStripMenuItem4.Size = New System.Drawing.Size(163, 22)
        Me.ToolStripMenuItem4.Text = "Day to Day Srock"
        Me.ToolStripMenuItem4.Visible = False
        '
        'StockReportsToolStripMenuItem
        '
        Me.StockReportsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnu_daily_stock, Me.mnu_monthly_stock, Me.mnu_d2d_stock})
        Me.StockReportsToolStripMenuItem.Name = "StockReportsToolStripMenuItem"
        Me.StockReportsToolStripMenuItem.Size = New System.Drawing.Size(252, 22)
        Me.StockReportsToolStripMenuItem.Text = "Stock Report - Category Wise"
        '
        'mnu_daily_stock
        '
        Me.mnu_daily_stock.Name = "mnu_daily_stock"
        Me.mnu_daily_stock.Size = New System.Drawing.Size(163, 22)
        Me.mnu_daily_stock.Text = "Daily Stock"
        Me.mnu_daily_stock.Visible = False
        '
        'mnu_monthly_stock
        '
        Me.mnu_monthly_stock.Name = "mnu_monthly_stock"
        Me.mnu_monthly_stock.Size = New System.Drawing.Size(163, 22)
        Me.mnu_monthly_stock.Text = "Monthly Stock"
        Me.mnu_monthly_stock.Visible = False
        '
        'mnu_d2d_stock
        '
        Me.mnu_d2d_stock.Name = "mnu_d2d_stock"
        Me.mnu_d2d_stock.Size = New System.Drawing.Size(163, 22)
        Me.mnu_d2d_stock.Text = "Day to Day Srock"
        Me.mnu_d2d_stock.Visible = False
        '
        'StockByMLToolStripMenuItem
        '
        Me.StockByMLToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem11, Me.ToolStripMenuItem15, Me.ToolStripMenuItem16})
        Me.StockByMLToolStripMenuItem.Name = "StockByMLToolStripMenuItem"
        Me.StockByMLToolStripMenuItem.Size = New System.Drawing.Size(252, 22)
        Me.StockByMLToolStripMenuItem.Text = "Stock Report - ML Wise"
        '
        'ToolStripMenuItem11
        '
        Me.ToolStripMenuItem11.Name = "ToolStripMenuItem11"
        Me.ToolStripMenuItem11.Size = New System.Drawing.Size(163, 22)
        Me.ToolStripMenuItem11.Text = "Daily Stock"
        Me.ToolStripMenuItem11.Visible = False
        '
        'ToolStripMenuItem15
        '
        Me.ToolStripMenuItem15.Name = "ToolStripMenuItem15"
        Me.ToolStripMenuItem15.Size = New System.Drawing.Size(163, 22)
        Me.ToolStripMenuItem15.Text = "Monthly Stock"
        Me.ToolStripMenuItem15.Visible = False
        '
        'ToolStripMenuItem16
        '
        Me.ToolStripMenuItem16.Name = "ToolStripMenuItem16"
        Me.ToolStripMenuItem16.Size = New System.Drawing.Size(163, 22)
        Me.ToolStripMenuItem16.Text = "Day to Day Srock"
        Me.ToolStripMenuItem16.Visible = False
        '
        'ToolStripSeparator23
        '
        Me.ToolStripSeparator23.Name = "ToolStripSeparator23"
        Me.ToolStripSeparator23.Size = New System.Drawing.Size(159, 6)
        '
        'PurchaseReportToolStripMenuItem
        '
        Me.PurchaseReportToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PurchaseReportsToolStripMenuItem, Me.PurchaseByBillNoToolStripMenuItem, Me.PurchaseSummaryByBillNoToolStripMenuItem, Me.PurchaseByItemToolStripMenuItem})
        Me.PurchaseReportToolStripMenuItem.Name = "PurchaseReportToolStripMenuItem"
        Me.PurchaseReportToolStripMenuItem.Size = New System.Drawing.Size(162, 22)
        Me.PurchaseReportToolStripMenuItem.Text = "Purchase Report"
        '
        'PurchaseReportsToolStripMenuItem
        '
        Me.PurchaseReportsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnu_daily_purchase, Me.mnu_monthly_purchase, Me.mnu_d2d_purchase})
        Me.PurchaseReportsToolStripMenuItem.Name = "PurchaseReportsToolStripMenuItem"
        Me.PurchaseReportsToolStripMenuItem.Size = New System.Drawing.Size(230, 22)
        Me.PurchaseReportsToolStripMenuItem.Text = "Purchase Details By Supplier"
        '
        'mnu_daily_purchase
        '
        Me.mnu_daily_purchase.Name = "mnu_daily_purchase"
        Me.mnu_daily_purchase.Size = New System.Drawing.Size(182, 22)
        Me.mnu_daily_purchase.Text = "Daily Purchase"
        Me.mnu_daily_purchase.Visible = False
        '
        'mnu_monthly_purchase
        '
        Me.mnu_monthly_purchase.Name = "mnu_monthly_purchase"
        Me.mnu_monthly_purchase.Size = New System.Drawing.Size(182, 22)
        Me.mnu_monthly_purchase.Text = "Monthly Purchase"
        Me.mnu_monthly_purchase.Visible = False
        '
        'mnu_d2d_purchase
        '
        Me.mnu_d2d_purchase.Name = "mnu_d2d_purchase"
        Me.mnu_d2d_purchase.Size = New System.Drawing.Size(182, 22)
        Me.mnu_d2d_purchase.Text = "Day to Day Purchase"
        Me.mnu_d2d_purchase.Visible = False
        '
        'PurchaseByBillNoToolStripMenuItem
        '
        Me.PurchaseByBillNoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem14, Me.ToolStripMenuItem18, Me.ToolStripMenuItem19})
        Me.PurchaseByBillNoToolStripMenuItem.Name = "PurchaseByBillNoToolStripMenuItem"
        Me.PurchaseByBillNoToolStripMenuItem.Size = New System.Drawing.Size(230, 22)
        Me.PurchaseByBillNoToolStripMenuItem.Text = "Purchase Details By Bill No."
        '
        'ToolStripMenuItem14
        '
        Me.ToolStripMenuItem14.Name = "ToolStripMenuItem14"
        Me.ToolStripMenuItem14.Size = New System.Drawing.Size(182, 22)
        Me.ToolStripMenuItem14.Text = "Daily Purchase"
        Me.ToolStripMenuItem14.Visible = False
        '
        'ToolStripMenuItem18
        '
        Me.ToolStripMenuItem18.Name = "ToolStripMenuItem18"
        Me.ToolStripMenuItem18.Size = New System.Drawing.Size(182, 22)
        Me.ToolStripMenuItem18.Text = "Monthly Purchase"
        Me.ToolStripMenuItem18.Visible = False
        '
        'ToolStripMenuItem19
        '
        Me.ToolStripMenuItem19.Name = "ToolStripMenuItem19"
        Me.ToolStripMenuItem19.Size = New System.Drawing.Size(182, 22)
        Me.ToolStripMenuItem19.Text = "Day to Day Purchase"
        Me.ToolStripMenuItem19.Visible = False
        '
        'PurchaseSummaryByBillNoToolStripMenuItem
        '
        Me.PurchaseSummaryByBillNoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem20, Me.ToolStripMenuItem24, Me.ToolStripMenuItem25})
        Me.PurchaseSummaryByBillNoToolStripMenuItem.Name = "PurchaseSummaryByBillNoToolStripMenuItem"
        Me.PurchaseSummaryByBillNoToolStripMenuItem.Size = New System.Drawing.Size(230, 22)
        Me.PurchaseSummaryByBillNoToolStripMenuItem.Text = "Purchase Summary By Bill No"
        '
        'ToolStripMenuItem20
        '
        Me.ToolStripMenuItem20.Name = "ToolStripMenuItem20"
        Me.ToolStripMenuItem20.Size = New System.Drawing.Size(182, 22)
        Me.ToolStripMenuItem20.Text = "Daily Purchase"
        Me.ToolStripMenuItem20.Visible = False
        '
        'ToolStripMenuItem24
        '
        Me.ToolStripMenuItem24.Name = "ToolStripMenuItem24"
        Me.ToolStripMenuItem24.Size = New System.Drawing.Size(182, 22)
        Me.ToolStripMenuItem24.Text = "Monthly Purchase"
        Me.ToolStripMenuItem24.Visible = False
        '
        'ToolStripMenuItem25
        '
        Me.ToolStripMenuItem25.Name = "ToolStripMenuItem25"
        Me.ToolStripMenuItem25.Size = New System.Drawing.Size(182, 22)
        Me.ToolStripMenuItem25.Text = "Day to Day Purchase"
        Me.ToolStripMenuItem25.Visible = False
        '
        'PurchaseByItemToolStripMenuItem
        '
        Me.PurchaseByItemToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem17, Me.ToolStripMenuItem21, Me.ToolStripMenuItem22})
        Me.PurchaseByItemToolStripMenuItem.Name = "PurchaseByItemToolStripMenuItem"
        Me.PurchaseByItemToolStripMenuItem.Size = New System.Drawing.Size(230, 22)
        Me.PurchaseByItemToolStripMenuItem.Text = "Purchase Summary By Item"
        '
        'ToolStripMenuItem17
        '
        Me.ToolStripMenuItem17.Name = "ToolStripMenuItem17"
        Me.ToolStripMenuItem17.Size = New System.Drawing.Size(182, 22)
        Me.ToolStripMenuItem17.Text = "Daily Purchase"
        Me.ToolStripMenuItem17.Visible = False
        '
        'ToolStripMenuItem21
        '
        Me.ToolStripMenuItem21.Name = "ToolStripMenuItem21"
        Me.ToolStripMenuItem21.Size = New System.Drawing.Size(182, 22)
        Me.ToolStripMenuItem21.Text = "Monthly Purchase"
        Me.ToolStripMenuItem21.Visible = False
        '
        'ToolStripMenuItem22
        '
        Me.ToolStripMenuItem22.Name = "ToolStripMenuItem22"
        Me.ToolStripMenuItem22.Size = New System.Drawing.Size(182, 22)
        Me.ToolStripMenuItem22.Text = "Day to Day Purchase"
        Me.ToolStripMenuItem22.Visible = False
        '
        'ToolStripSeparator24
        '
        Me.ToolStripSeparator24.Name = "ToolStripSeparator24"
        Me.ToolStripSeparator24.Size = New System.Drawing.Size(159, 6)
        '
        'BreakageReportToolStripMenuItem
        '
        Me.BreakageReportToolStripMenuItem.Name = "BreakageReportToolStripMenuItem"
        Me.BreakageReportToolStripMenuItem.Size = New System.Drawing.Size(162, 22)
        Me.BreakageReportToolStripMenuItem.Text = "Breakage Report"
        '
        'ToolStripSeparator13
        '
        Me.ToolStripSeparator13.Name = "ToolStripSeparator13"
        Me.ToolStripSeparator13.Size = New System.Drawing.Size(159, 6)
        '
        'ExciseReportsToolStripMenuItem
        '
        Me.ExciseReportsToolStripMenuItem.Name = "ExciseReportsToolStripMenuItem"
        Me.ExciseReportsToolStripMenuItem.Size = New System.Drawing.Size(162, 22)
        Me.ExciseReportsToolStripMenuItem.Text = "Excise Reports"
        '
        'ToolStripSeparator29
        '
        Me.ToolStripSeparator29.Name = "ToolStripSeparator29"
        Me.ToolStripSeparator29.Size = New System.Drawing.Size(159, 6)
        '
        'BulkLtrToolStripMenuItem
        '
        Me.BulkLtrToolStripMenuItem.Name = "BulkLtrToolStripMenuItem"
        Me.BulkLtrToolStripMenuItem.Size = New System.Drawing.Size(162, 22)
        Me.BulkLtrToolStripMenuItem.Text = "Bulk Ltr"
        '
        'ToolStripSeparator32
        '
        Me.ToolStripSeparator32.Name = "ToolStripSeparator32"
        Me.ToolStripSeparator32.Size = New System.Drawing.Size(159, 6)
        '
        'AccountsReportToolStripMenuItem
        '
        Me.AccountsReportToolStripMenuItem.Name = "AccountsReportToolStripMenuItem"
        Me.AccountsReportToolStripMenuItem.Size = New System.Drawing.Size(162, 22)
        Me.AccountsReportToolStripMenuItem.Text = "Accounts Report"
        '
        'TransactionsToolStripMenuItem
        '
        Me.TransactionsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CounterSaleToolStripMenuItem, Me.ToolStripSeparator20, Me.PurchaseBillToolStripMenuItem, Me.ToolStripSeparator21, Me.PaymentVoucherToolStripMenuItem1, Me.PaymentVoucherToolStripMenuItem, Me.ToolStripSeparator22, Me.OpeningStockToolStripMenuItem1, Me.ToolStripSeparator25, Me.StockTransferToolStripMenuItem, Me.BeakageEntryToolStripMenuItem})
        Me.TransactionsToolStripMenuItem.Name = "TransactionsToolStripMenuItem"
        Me.TransactionsToolStripMenuItem.Size = New System.Drawing.Size(86, 20)
        Me.TransactionsToolStripMenuItem.Text = "&Transactions"
        '
        'CounterSaleToolStripMenuItem
        '
        Me.CounterSaleToolStripMenuItem.Name = "CounterSaleToolStripMenuItem"
        Me.CounterSaleToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.CounterSaleToolStripMenuItem.Text = "Sale"
        '
        'ToolStripSeparator20
        '
        Me.ToolStripSeparator20.Name = "ToolStripSeparator20"
        Me.ToolStripSeparator20.Size = New System.Drawing.Size(165, 6)
        '
        'PurchaseBillToolStripMenuItem
        '
        Me.PurchaseBillToolStripMenuItem.Name = "PurchaseBillToolStripMenuItem"
        Me.PurchaseBillToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.PurchaseBillToolStripMenuItem.Text = "Purchase"
        '
        'ToolStripSeparator21
        '
        Me.ToolStripSeparator21.Name = "ToolStripSeparator21"
        Me.ToolStripSeparator21.Size = New System.Drawing.Size(165, 6)
        '
        'PaymentVoucherToolStripMenuItem1
        '
        Me.PaymentVoucherToolStripMenuItem1.Name = "PaymentVoucherToolStripMenuItem1"
        Me.PaymentVoucherToolStripMenuItem1.Size = New System.Drawing.Size(168, 22)
        Me.PaymentVoucherToolStripMenuItem1.Text = "Payment Voucher"
        '
        'PaymentVoucherToolStripMenuItem
        '
        Me.PaymentVoucherToolStripMenuItem.Name = "PaymentVoucherToolStripMenuItem"
        Me.PaymentVoucherToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.PaymentVoucherToolStripMenuItem.Text = "Receipt Voucher"
        '
        'ToolStripSeparator22
        '
        Me.ToolStripSeparator22.Name = "ToolStripSeparator22"
        Me.ToolStripSeparator22.Size = New System.Drawing.Size(165, 6)
        '
        'OpeningStockToolStripMenuItem1
        '
        Me.OpeningStockToolStripMenuItem1.Name = "OpeningStockToolStripMenuItem1"
        Me.OpeningStockToolStripMenuItem1.Size = New System.Drawing.Size(168, 22)
        Me.OpeningStockToolStripMenuItem1.Text = "Opening Stock"
        '
        'ToolStripSeparator25
        '
        Me.ToolStripSeparator25.Name = "ToolStripSeparator25"
        Me.ToolStripSeparator25.Size = New System.Drawing.Size(165, 6)
        '
        'StockTransferToolStripMenuItem
        '
        Me.StockTransferToolStripMenuItem.Name = "StockTransferToolStripMenuItem"
        Me.StockTransferToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.StockTransferToolStripMenuItem.Text = "Stock Transfer"
        Me.StockTransferToolStripMenuItem.Visible = False
        '
        'BeakageEntryToolStripMenuItem
        '
        Me.BeakageEntryToolStripMenuItem.Name = "BeakageEntryToolStripMenuItem"
        Me.BeakageEntryToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.BeakageEntryToolStripMenuItem.Text = "Beakage Entry"
        '
        'TallyImportToolStripMenuItem
        '
        Me.TallyImportToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ImportSalesToolStripMenuItem, Me.ImportPurchaseToolStripMenuItem})
        Me.TallyImportToolStripMenuItem.Name = "TallyImportToolStripMenuItem"
        Me.TallyImportToolStripMenuItem.Size = New System.Drawing.Size(83, 20)
        Me.TallyImportToolStripMenuItem.Text = "Tally Import"
        '
        'ImportSalesToolStripMenuItem
        '
        Me.ImportSalesToolStripMenuItem.Name = "ImportSalesToolStripMenuItem"
        Me.ImportSalesToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.ImportSalesToolStripMenuItem.Text = "Import Sales"
        '
        'ImportPurchaseToolStripMenuItem
        '
        Me.ImportPurchaseToolStripMenuItem.Name = "ImportPurchaseToolStripMenuItem"
        Me.ImportPurchaseToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.ImportPurchaseToolStripMenuItem.Text = "Import Purchase"
        '
        'SETTINGSToolStripMenuItem
        '
        Me.SETTINGSToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CalculatorToolStripMenuItem, Me.ToolStripSeparator34, Me.CREATESHORTCUTToolStripMenuItem, Me.ToolStripSeparator35, Me.CHANGEIDToolStripMenuItem, Me.ToolStripSeparator36, Me.HelpToolStripMenuItem, Me.ToolStripSeparator37, Me.BackUpToolStripMenuItem, Me.ToolStripSeparator33, Me.DataFetchToolStripMenuItem, Me.ToolStripSeparator31, Me.SendSmsToolStripMenuItem})
        Me.SETTINGSToolStripMenuItem.Name = "SETTINGSToolStripMenuItem"
        Me.SETTINGSToolStripMenuItem.Size = New System.Drawing.Size(61, 20)
        Me.SETTINGSToolStripMenuItem.Text = "&Settings"
        '
        'CalculatorToolStripMenuItem
        '
        Me.CalculatorToolStripMenuItem.Name = "CalculatorToolStripMenuItem"
        Me.CalculatorToolStripMenuItem.Size = New System.Drawing.Size(175, 22)
        Me.CalculatorToolStripMenuItem.Text = "Calculator"
        '
        'ToolStripSeparator34
        '
        Me.ToolStripSeparator34.Name = "ToolStripSeparator34"
        Me.ToolStripSeparator34.Size = New System.Drawing.Size(172, 6)
        '
        'CREATESHORTCUTToolStripMenuItem
        '
        Me.CREATESHORTCUTToolStripMenuItem.Name = "CREATESHORTCUTToolStripMenuItem"
        Me.CREATESHORTCUTToolStripMenuItem.Size = New System.Drawing.Size(175, 22)
        Me.CREATESHORTCUTToolStripMenuItem.Text = "Access Bar"
        Me.CREATESHORTCUTToolStripMenuItem.Visible = False
        '
        'ToolStripSeparator35
        '
        Me.ToolStripSeparator35.Name = "ToolStripSeparator35"
        Me.ToolStripSeparator35.Size = New System.Drawing.Size(172, 6)
        '
        'CHANGEIDToolStripMenuItem
        '
        Me.CHANGEIDToolStripMenuItem.Name = "CHANGEIDToolStripMenuItem"
        Me.CHANGEIDToolStripMenuItem.Size = New System.Drawing.Size(175, 22)
        Me.CHANGEIDToolStripMenuItem.Text = "Change Paasword"
        '
        'ToolStripSeparator36
        '
        Me.ToolStripSeparator36.Name = "ToolStripSeparator36"
        Me.ToolStripSeparator36.Size = New System.Drawing.Size(172, 6)
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(175, 22)
        Me.HelpToolStripMenuItem.Text = "About Madhushala"
        '
        'ToolStripSeparator37
        '
        Me.ToolStripSeparator37.Name = "ToolStripSeparator37"
        Me.ToolStripSeparator37.Size = New System.Drawing.Size(172, 6)
        '
        'BackUpToolStripMenuItem
        '
        Me.BackUpToolStripMenuItem.Name = "BackUpToolStripMenuItem"
        Me.BackUpToolStripMenuItem.Size = New System.Drawing.Size(175, 22)
        Me.BackUpToolStripMenuItem.Text = "Data Back-Up"
        '
        'ToolStripSeparator33
        '
        Me.ToolStripSeparator33.Name = "ToolStripSeparator33"
        Me.ToolStripSeparator33.Size = New System.Drawing.Size(172, 6)
        '
        'DataFetchToolStripMenuItem
        '
        Me.DataFetchToolStripMenuItem.Name = "DataFetchToolStripMenuItem"
        Me.DataFetchToolStripMenuItem.Size = New System.Drawing.Size(175, 22)
        Me.DataFetchToolStripMenuItem.Text = "Data Import"
        '
        'ToolStripSeparator31
        '
        Me.ToolStripSeparator31.Name = "ToolStripSeparator31"
        Me.ToolStripSeparator31.Size = New System.Drawing.Size(172, 6)
        '
        'SendSmsToolStripMenuItem
        '
        Me.SendSmsToolStripMenuItem.Name = "SendSmsToolStripMenuItem"
        Me.SendSmsToolStripMenuItem.Size = New System.Drawing.Size(175, 22)
        Me.SendSmsToolStripMenuItem.Text = "Send SMS"
        '
        'UpgradeToolStripMenuItem
        '
        Me.UpgradeToolStripMenuItem.Name = "UpgradeToolStripMenuItem"
        Me.UpgradeToolStripMenuItem.Size = New System.Drawing.Size(64, 20)
        Me.UpgradeToolStripMenuItem.Text = "Upgrade"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.BackColor = System.Drawing.Color.NavajoWhite
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1, Me.year_drop, Me.ToolStripStatusLabel6, Me.ToolStripStatusLabel2, Me.ToolStripStatusLabel3, Me.ToolStripStatusLabel4, Me.ToolStripStatusLabel5})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 686)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(1018, 22)
        Me.StatusStrip1.TabIndex = 12
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.AutoSize = False
        Me.ToolStripStatusLabel1.BackColor = System.Drawing.Color.Transparent
        Me.ToolStripStatusLabel1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(170, 17)
        '
        'year_drop
        '
        Me.year_drop.AutoSize = False
        Me.year_drop.BackColor = System.Drawing.Color.Transparent
        Me.year_drop.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.year_drop.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.year_drop.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.year_drop.Name = "year_drop"
        Me.year_drop.Size = New System.Drawing.Size(100, 20)
        '
        'ToolStripStatusLabel6
        '
        Me.ToolStripStatusLabel6.AutoSize = False
        Me.ToolStripStatusLabel6.BackColor = System.Drawing.Color.Transparent
        Me.ToolStripStatusLabel6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.ToolStripStatusLabel6.Name = "ToolStripStatusLabel6"
        Me.ToolStripStatusLabel6.Size = New System.Drawing.Size(500, 17)
        '
        'ToolStripStatusLabel2
        '
        Me.ToolStripStatusLabel2.AutoSize = False
        Me.ToolStripStatusLabel2.BackColor = System.Drawing.Color.Transparent
        Me.ToolStripStatusLabel2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2"
        Me.ToolStripStatusLabel2.Size = New System.Drawing.Size(40, 17)
        Me.ToolStripStatusLabel2.Text = "Time :   "
        Me.ToolStripStatusLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'ToolStripStatusLabel3
        '
        Me.ToolStripStatusLabel3.AutoSize = False
        Me.ToolStripStatusLabel3.BackColor = System.Drawing.Color.Transparent
        Me.ToolStripStatusLabel3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.ToolStripStatusLabel3.Name = "ToolStripStatusLabel3"
        Me.ToolStripStatusLabel3.Size = New System.Drawing.Size(75, 17)
        Me.ToolStripStatusLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'ToolStripStatusLabel4
        '
        Me.ToolStripStatusLabel4.AutoSize = False
        Me.ToolStripStatusLabel4.BackColor = System.Drawing.Color.Transparent
        Me.ToolStripStatusLabel4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.ToolStripStatusLabel4.Name = "ToolStripStatusLabel4"
        Me.ToolStripStatusLabel4.Size = New System.Drawing.Size(40, 17)
        Me.ToolStripStatusLabel4.Text = "Date :   "
        Me.ToolStripStatusLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'ToolStripStatusLabel5
        '
        Me.ToolStripStatusLabel5.AutoSize = False
        Me.ToolStripStatusLabel5.BackColor = System.Drawing.Color.Transparent
        Me.ToolStripStatusLabel5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.ToolStripStatusLabel5.Name = "ToolStripStatusLabel5"
        Me.ToolStripStatusLabel5.Size = New System.Drawing.Size(75, 17)
        Me.ToolStripStatusLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 1000
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.SystemColors.ActiveBorder
        Me.Button1.Dock = System.Windows.Forms.DockStyle.Left
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(150, 52)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(20, 634)
        Me.Button1.TabIndex = 16
        Me.Button1.Text = "<"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'ToolStrip2
        '
        Me.ToolStrip2.AutoSize = False
        Me.ToolStrip2.BackColor = System.Drawing.Color.NavajoWhite
        Me.ToolStrip2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ToolStrip2.Dock = System.Windows.Forms.DockStyle.Left
        Me.ToolStrip2.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.ToolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ToolStrip2.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_companyinfo, Me.ToolStripLabel5, Me.ToolStripLabel6, Me.ToolStripLabel7, Me.ToolStripLabel8, Me.ToolStripLabel9, Me.ToolStripLabel10, Me.ToolStripLabel11, Me.ToolStripLabel12, Me.ToolStripLabel13, Me.ToolStripLabel14, Me.ToolStripLabel15, Me.ToolStripLabel16, Me.ToolStripLabel17, Me.ToolStripLabel18, Me.ToolStripLabel19, Me.ToolStripLabel20, Me.ToolStripLabel21, Me.ToolStripLabel22, Me.ToolStripLabel23})
        Me.ToolStrip2.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.VerticalStackWithOverflow
        Me.ToolStrip2.Location = New System.Drawing.Point(0, 52)
        Me.ToolStrip2.Name = "ToolStrip2"
        Me.ToolStrip2.Size = New System.Drawing.Size(150, 634)
        Me.ToolStrip2.TabIndex = 15
        Me.ToolStrip2.Text = "ToolStrip2"
        '
        'ts_companyinfo
        '
        Me.ts_companyinfo.AutoSize = False
        Me.ts_companyinfo.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.ts_companyinfo.Image = CType(resources.GetObject("ts_companyinfo.Image"), System.Drawing.Image)
        Me.ts_companyinfo.ImageTransparentColor = System.Drawing.Color.Transparent
        Me.ts_companyinfo.Name = "ts_companyinfo"
        Me.ts_companyinfo.Size = New System.Drawing.Size(148, 25)
        Me.ts_companyinfo.Text = "Company Information"
        Me.ts_companyinfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ts_companyinfo.Visible = False
        '
        'ToolStripLabel5
        '
        Me.ToolStripLabel5.AutoSize = False
        Me.ToolStripLabel5.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.ToolStripLabel5.Image = CType(resources.GetObject("ToolStripLabel5.Image"), System.Drawing.Image)
        Me.ToolStripLabel5.ImageTransparentColor = System.Drawing.Color.Transparent
        Me.ToolStripLabel5.Name = "ToolStripLabel5"
        Me.ToolStripLabel5.Size = New System.Drawing.Size(148, 25)
        Me.ToolStripLabel5.Text = "ToolStripLabel5"
        Me.ToolStripLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ToolStripLabel5.Visible = False
        '
        'ToolStripLabel6
        '
        Me.ToolStripLabel6.AutoSize = False
        Me.ToolStripLabel6.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.ToolStripLabel6.Image = CType(resources.GetObject("ToolStripLabel6.Image"), System.Drawing.Image)
        Me.ToolStripLabel6.ImageTransparentColor = System.Drawing.Color.Transparent
        Me.ToolStripLabel6.Name = "ToolStripLabel6"
        Me.ToolStripLabel6.Size = New System.Drawing.Size(148, 25)
        Me.ToolStripLabel6.Text = "ToolStripLabel6"
        Me.ToolStripLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ToolStripLabel6.Visible = False
        '
        'ToolStripLabel7
        '
        Me.ToolStripLabel7.AutoSize = False
        Me.ToolStripLabel7.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.ToolStripLabel7.Image = CType(resources.GetObject("ToolStripLabel7.Image"), System.Drawing.Image)
        Me.ToolStripLabel7.ImageTransparentColor = System.Drawing.Color.Transparent
        Me.ToolStripLabel7.Name = "ToolStripLabel7"
        Me.ToolStripLabel7.Size = New System.Drawing.Size(148, 25)
        Me.ToolStripLabel7.Text = "ToolStripLabel7"
        Me.ToolStripLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ToolStripLabel7.Visible = False
        '
        'ToolStripLabel8
        '
        Me.ToolStripLabel8.AutoSize = False
        Me.ToolStripLabel8.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.ToolStripLabel8.Image = CType(resources.GetObject("ToolStripLabel8.Image"), System.Drawing.Image)
        Me.ToolStripLabel8.ImageTransparentColor = System.Drawing.Color.Transparent
        Me.ToolStripLabel8.Name = "ToolStripLabel8"
        Me.ToolStripLabel8.Size = New System.Drawing.Size(148, 25)
        Me.ToolStripLabel8.Text = "ToolStripLabel8"
        Me.ToolStripLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ToolStripLabel8.Visible = False
        '
        'ToolStripLabel9
        '
        Me.ToolStripLabel9.AutoSize = False
        Me.ToolStripLabel9.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.ToolStripLabel9.Image = CType(resources.GetObject("ToolStripLabel9.Image"), System.Drawing.Image)
        Me.ToolStripLabel9.ImageTransparentColor = System.Drawing.Color.Transparent
        Me.ToolStripLabel9.Name = "ToolStripLabel9"
        Me.ToolStripLabel9.Size = New System.Drawing.Size(148, 25)
        Me.ToolStripLabel9.Text = "ToolStripLabel9"
        Me.ToolStripLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ToolStripLabel9.Visible = False
        '
        'ToolStripLabel10
        '
        Me.ToolStripLabel10.AutoSize = False
        Me.ToolStripLabel10.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.ToolStripLabel10.Image = CType(resources.GetObject("ToolStripLabel10.Image"), System.Drawing.Image)
        Me.ToolStripLabel10.ImageTransparentColor = System.Drawing.Color.Transparent
        Me.ToolStripLabel10.Name = "ToolStripLabel10"
        Me.ToolStripLabel10.Size = New System.Drawing.Size(148, 25)
        Me.ToolStripLabel10.Text = "ToolStripLabel10"
        Me.ToolStripLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ToolStripLabel10.Visible = False
        '
        'ToolStripLabel11
        '
        Me.ToolStripLabel11.AutoSize = False
        Me.ToolStripLabel11.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.ToolStripLabel11.Image = CType(resources.GetObject("ToolStripLabel11.Image"), System.Drawing.Image)
        Me.ToolStripLabel11.ImageTransparentColor = System.Drawing.Color.Transparent
        Me.ToolStripLabel11.Name = "ToolStripLabel11"
        Me.ToolStripLabel11.Size = New System.Drawing.Size(148, 25)
        Me.ToolStripLabel11.Text = "ToolStripLabel11"
        Me.ToolStripLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ToolStripLabel11.Visible = False
        '
        'ToolStripLabel12
        '
        Me.ToolStripLabel12.AutoSize = False
        Me.ToolStripLabel12.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.ToolStripLabel12.Image = CType(resources.GetObject("ToolStripLabel12.Image"), System.Drawing.Image)
        Me.ToolStripLabel12.ImageTransparentColor = System.Drawing.Color.Transparent
        Me.ToolStripLabel12.Name = "ToolStripLabel12"
        Me.ToolStripLabel12.Size = New System.Drawing.Size(148, 25)
        Me.ToolStripLabel12.Text = "ToolStripLabel12"
        Me.ToolStripLabel12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ToolStripLabel12.Visible = False
        '
        'ToolStripLabel13
        '
        Me.ToolStripLabel13.AutoSize = False
        Me.ToolStripLabel13.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.ToolStripLabel13.Image = CType(resources.GetObject("ToolStripLabel13.Image"), System.Drawing.Image)
        Me.ToolStripLabel13.ImageTransparentColor = System.Drawing.Color.Transparent
        Me.ToolStripLabel13.Name = "ToolStripLabel13"
        Me.ToolStripLabel13.Size = New System.Drawing.Size(148, 25)
        Me.ToolStripLabel13.Text = "ToolStripLabel13"
        Me.ToolStripLabel13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ToolStripLabel13.Visible = False
        '
        'ToolStripLabel14
        '
        Me.ToolStripLabel14.AutoSize = False
        Me.ToolStripLabel14.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.ToolStripLabel14.Image = CType(resources.GetObject("ToolStripLabel14.Image"), System.Drawing.Image)
        Me.ToolStripLabel14.ImageTransparentColor = System.Drawing.Color.Transparent
        Me.ToolStripLabel14.Name = "ToolStripLabel14"
        Me.ToolStripLabel14.Size = New System.Drawing.Size(148, 25)
        Me.ToolStripLabel14.Text = "ToolStripLabel14"
        Me.ToolStripLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ToolStripLabel14.Visible = False
        '
        'ToolStripLabel15
        '
        Me.ToolStripLabel15.AutoSize = False
        Me.ToolStripLabel15.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.ToolStripLabel15.Image = CType(resources.GetObject("ToolStripLabel15.Image"), System.Drawing.Image)
        Me.ToolStripLabel15.ImageTransparentColor = System.Drawing.Color.Transparent
        Me.ToolStripLabel15.Name = "ToolStripLabel15"
        Me.ToolStripLabel15.Size = New System.Drawing.Size(148, 25)
        Me.ToolStripLabel15.Text = "ToolStripLabel15"
        Me.ToolStripLabel15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ToolStripLabel15.Visible = False
        '
        'ToolStripLabel16
        '
        Me.ToolStripLabel16.AutoSize = False
        Me.ToolStripLabel16.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.ToolStripLabel16.Image = CType(resources.GetObject("ToolStripLabel16.Image"), System.Drawing.Image)
        Me.ToolStripLabel16.ImageTransparentColor = System.Drawing.Color.Transparent
        Me.ToolStripLabel16.Name = "ToolStripLabel16"
        Me.ToolStripLabel16.Size = New System.Drawing.Size(148, 25)
        Me.ToolStripLabel16.Text = "ToolStripLabel16"
        Me.ToolStripLabel16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ToolStripLabel16.Visible = False
        '
        'ToolStripLabel17
        '
        Me.ToolStripLabel17.AutoSize = False
        Me.ToolStripLabel17.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.ToolStripLabel17.Image = CType(resources.GetObject("ToolStripLabel17.Image"), System.Drawing.Image)
        Me.ToolStripLabel17.ImageTransparentColor = System.Drawing.Color.Transparent
        Me.ToolStripLabel17.Name = "ToolStripLabel17"
        Me.ToolStripLabel17.Size = New System.Drawing.Size(148, 25)
        Me.ToolStripLabel17.Text = "ToolStripLabel17"
        Me.ToolStripLabel17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ToolStripLabel17.Visible = False
        '
        'ToolStripLabel18
        '
        Me.ToolStripLabel18.AutoSize = False
        Me.ToolStripLabel18.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.ToolStripLabel18.Image = CType(resources.GetObject("ToolStripLabel18.Image"), System.Drawing.Image)
        Me.ToolStripLabel18.ImageTransparentColor = System.Drawing.Color.Transparent
        Me.ToolStripLabel18.Name = "ToolStripLabel18"
        Me.ToolStripLabel18.Size = New System.Drawing.Size(148, 25)
        Me.ToolStripLabel18.Text = "ToolStripLabel18"
        Me.ToolStripLabel18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ToolStripLabel18.Visible = False
        '
        'ToolStripLabel19
        '
        Me.ToolStripLabel19.AutoSize = False
        Me.ToolStripLabel19.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.ToolStripLabel19.Image = CType(resources.GetObject("ToolStripLabel19.Image"), System.Drawing.Image)
        Me.ToolStripLabel19.ImageTransparentColor = System.Drawing.Color.Transparent
        Me.ToolStripLabel19.Name = "ToolStripLabel19"
        Me.ToolStripLabel19.Size = New System.Drawing.Size(148, 25)
        Me.ToolStripLabel19.Text = "ToolStripLabel19"
        Me.ToolStripLabel19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ToolStripLabel19.Visible = False
        '
        'ToolStripLabel20
        '
        Me.ToolStripLabel20.AutoSize = False
        Me.ToolStripLabel20.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.ToolStripLabel20.Image = CType(resources.GetObject("ToolStripLabel20.Image"), System.Drawing.Image)
        Me.ToolStripLabel20.ImageTransparentColor = System.Drawing.Color.Transparent
        Me.ToolStripLabel20.Name = "ToolStripLabel20"
        Me.ToolStripLabel20.Size = New System.Drawing.Size(148, 25)
        Me.ToolStripLabel20.Text = "ToolStripLabel20"
        Me.ToolStripLabel20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ToolStripLabel20.Visible = False
        '
        'ToolStripLabel21
        '
        Me.ToolStripLabel21.AutoSize = False
        Me.ToolStripLabel21.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.ToolStripLabel21.Image = CType(resources.GetObject("ToolStripLabel21.Image"), System.Drawing.Image)
        Me.ToolStripLabel21.ImageTransparentColor = System.Drawing.Color.Transparent
        Me.ToolStripLabel21.Name = "ToolStripLabel21"
        Me.ToolStripLabel21.Size = New System.Drawing.Size(148, 25)
        Me.ToolStripLabel21.Text = "ToolStripLabel21"
        Me.ToolStripLabel21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ToolStripLabel21.Visible = False
        '
        'ToolStripLabel22
        '
        Me.ToolStripLabel22.AutoSize = False
        Me.ToolStripLabel22.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.ToolStripLabel22.Image = CType(resources.GetObject("ToolStripLabel22.Image"), System.Drawing.Image)
        Me.ToolStripLabel22.ImageTransparentColor = System.Drawing.Color.Transparent
        Me.ToolStripLabel22.Name = "ToolStripLabel22"
        Me.ToolStripLabel22.Size = New System.Drawing.Size(148, 25)
        Me.ToolStripLabel22.Text = "ToolStripLabel22"
        Me.ToolStripLabel22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ToolStripLabel22.Visible = False
        '
        'ToolStripLabel23
        '
        Me.ToolStripLabel23.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripLabel23.AutoSize = False
        Me.ToolStripLabel23.BackColor = System.Drawing.Color.Transparent
        Me.ToolStripLabel23.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ToolStripLabel23.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripLabel23.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.ToolStripLabel23.ImageTransparentColor = System.Drawing.Color.White
        Me.ToolStripLabel23.Name = "ToolStripLabel23"
        Me.ToolStripLabel23.Size = New System.Drawing.Size(50, 50)
        Me.ToolStripLabel23.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        Me.ToolStripLabel23.ToolTipText = "About Madhushala"
        '
        'ToolStripSeparator12
        '
        Me.ToolStripSeparator12.Name = "ToolStripSeparator12"
        Me.ToolStripSeparator12.Size = New System.Drawing.Size(6, 28)
        '
        'mnu_closing_stock
        '
        Me.mnu_closing_stock.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.mnu_closing_stock.AutoSize = False
        Me.mnu_closing_stock.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.mnu_closing_stock.Name = "mnu_closing_stock"
        Me.mnu_closing_stock.Size = New System.Drawing.Size(400, 22)
        '
        'ToolStripLabel2
        '
        Me.ToolStripLabel2.AutoSize = False
        Me.ToolStripLabel2.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.ToolStripLabel2.Name = "ToolStripLabel2"
        Me.ToolStripLabel2.Size = New System.Drawing.Size(200, 22)
        Me.ToolStripLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ToolStripSeparator14
        '
        Me.ToolStripSeparator14.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripSeparator14.Name = "ToolStripSeparator14"
        Me.ToolStripSeparator14.Size = New System.Drawing.Size(6, 28)
        '
        'ToolStripSeparator15
        '
        Me.ToolStripSeparator15.Name = "ToolStripSeparator15"
        Me.ToolStripSeparator15.Size = New System.Drawing.Size(6, 28)
        '
        'ToolStripLabel1
        '
        Me.ToolStripLabel1.AutoSize = False
        Me.ToolStripLabel1.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.ToolStripLabel1.Name = "ToolStripLabel1"
        Me.ToolStripLabel1.Size = New System.Drawing.Size(200, 22)
        '
        'ToolStrip1
        '
        Me.ToolStrip1.BackColor = System.Drawing.SystemColors.ActiveBorder
        Me.ToolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.ToolStripButton2, Me.ToolStripButton3, Me.ToolStripSeparator12, Me.mnu_closing_stock, Me.ToolStripLabel2, Me.ToolStripSeparator14, Me.ToolStripSeparator15, Me.ToolStripLabel1})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 24)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.ToolStrip1.Size = New System.Drawing.Size(1018, 28)
        Me.ToolStrip1.TabIndex = 1
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.AutoSize = False
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Transparent
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(25, 25)
        Me.ToolStripButton1.Text = "ToolStripButton1"
        Me.ToolStripButton1.ToolTipText = "Log-In"
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.AutoSize = False
        Me.ToolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton2.Image = CType(resources.GetObject("ToolStripButton2.Image"), System.Drawing.Image)
        Me.ToolStripButton2.ImageTransparentColor = System.Drawing.Color.Transparent
        Me.ToolStripButton2.Name = "ToolStripButton2"
        Me.ToolStripButton2.Size = New System.Drawing.Size(25, 25)
        Me.ToolStripButton2.Text = "ToolStripButton2"
        Me.ToolStripButton2.ToolTipText = "Log-Out"
        Me.ToolStripButton2.Visible = False
        '
        'ToolStripButton3
        '
        Me.ToolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton3.Image = CType(resources.GetObject("ToolStripButton3.Image"), System.Drawing.Image)
        Me.ToolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton3.Name = "ToolStripButton3"
        Me.ToolStripButton3.Size = New System.Drawing.Size(23, 25)
        Me.ToolStripButton3.Text = "ToolStripButton3"
        Me.ToolStripButton3.ToolTipText = "Change Company"
        Me.ToolStripButton3.Visible = False
        '
        'frm_ContainerForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(1018, 708)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.ToolStrip2)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.Name = "frm_ContainerForm"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ToolStrip2.ResumeLayout(False)
        Me.ToolStrip2.PerformLayout()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ContentsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IndexToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SearchToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents toolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents AboutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NewToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OpenToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents toolStripSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents SaveToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SaveAsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents toolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents PrintToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PrintPreviewToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents toolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UndoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RedoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents toolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CopyToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PasteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents toolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents SelectAllToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CustomizeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OptionsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NewToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OpenToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents toolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents SaveToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SaveAsToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents toolStripSeparator7 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents PrintToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PrintPreviewToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents toolStripSeparator8 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ExitToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UndoToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RedoToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents toolStripSeparator9 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CutToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CopyToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PasteToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents toolStripSeparator10 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents SelectAllToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CustomizeToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OptionsToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContentsToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IndexToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SearchToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents toolStripSeparator11 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents AboutToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FILEToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents REPORTToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GETEXCELLToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GETWORDDOCUMENTToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GETTXTBYTRNNOToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GETTXTBYDATEToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MasterToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BrandMasterToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents KindOfForeignLoquoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MeasurePackingToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ItemCateToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ItemMasterToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SETTINGSToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CHANGEIDToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CREATESHORTCUTToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AccountCreationToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SalesRateToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TaxShemesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StorageLocationToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StrengthToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AcountSubGroupToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel2 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel3 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel4 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents AdminToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TransactionsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CalculatorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripStatusLabel5 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel6 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents MainToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CREATECOMPANYToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UserInformationToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CompanyParameterToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LogToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LogOutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents year_drop As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents LedgerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator16 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator17 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator18 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents UserRightsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator19 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents NeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CounterSaleToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OpeningStockToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PurchaseBillToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PaymentVoucherToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator21 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents StockTransferToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BeakageEntryToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents ToolStrip2 As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStripLabel23 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripLabel29 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents HelpToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator22 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator12 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnu_closing_stock As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripLabel2 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripSeparator14 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator15 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripLabel1 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton2 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton3 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_companyinfo As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripLabel5 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripLabel6 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripLabel7 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripLabel8 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripLabel9 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripLabel10 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripLabel11 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripLabel12 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripLabel13 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripLabel14 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripLabel15 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripLabel16 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripLabel17 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripLabel18 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripLabel19 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripLabel20 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripLabel21 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripLabel22 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ChangeCompanyToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ChangeServerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator13 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator23 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator24 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents PaymentVoucherToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator25 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator20 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator26 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator27 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator28 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents DataFetchToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator29 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents AccountsReportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExciseReportsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BreakageReportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator30 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents SalesReportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SaleReportsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnu_daily_sale As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnu_monthly_sale As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnu_d2d_sale As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnu_multybill_print As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem26 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem42 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem43 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem44 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem27 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem28 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem29 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem30 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem23 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem39 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem40 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem41 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem31 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem32 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem33 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem34 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem35 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem36 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem37 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem38 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SalesReturnToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StockReportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StockByItemsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem8 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem12 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem13 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StockReportValueWiseClosingToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StockReportValueWiseDetailToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StocksByBrandToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem3 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem4 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StockReportsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnu_daily_stock As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnu_monthly_stock As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnu_d2d_stock As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StockByMLToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem11 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem15 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem16 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PurchaseReportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PurchaseReportsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnu_daily_purchase As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnu_monthly_purchase As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnu_d2d_purchase As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PurchaseByBillNoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem14 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem18 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem19 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PurchaseSummaryByBillNoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem20 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem24 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem25 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PurchaseByItemToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem17 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem21 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem22 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BackUpToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UpgradeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator32 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BulkLtrToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SendSmsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator31 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator34 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator35 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator36 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator37 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator33 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents TallyImportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ImportSalesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ImportPurchaseToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem

End Class
